
import os
import glob
import sys

sys.path.insert(0, './python-modules/')

from jinja2 import Environment, FileSystemLoader


DIR = os.path.join(os.path.abspath(os.path.dirname(__file__)))
PUBLIC_DIR = os.path.join(DIR, 'public')
env = Environment(loader=FileSystemLoader(os.path.join(DIR, 'templates')))


for template_path in glob.glob('%s/*.html' % os.path.join(DIR, 'templates/pages')):
    template_name = os.path.basename(template_path)
    template = env.get_template('pages/%s' % template_name)
    f = open(os.path.join(PUBLIC_DIR, template_name), 'w')
    html = template.render()
    f.write(html.encode('utf-8'))
    f.close()
